FROM gitlab-registry.cern.ch/linuxsupport/c8-base

ADD etc/openafs-testing.repo /etc/yum.repos.d/
ADD etc/htcondor-stable.repo /etc/yum.repos.d/
ADD etc/epel.repo /etc/yum.repos.d/
RUN dnf install -y python3 python3-psutil python3-gssapi python3-devel openafs-krb5 git make gcc which
RUN dnf install -y python3-condor condor condor-external-libs condor-classads condor-procd openldap-devel

ADD etc/krb5.conf /etc/

RUN git clone https://github.com/StackStorm/st2.git
WORKDIR "/st2"
RUN git submodule update --init --recursive
RUN  make requirements
